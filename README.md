# EmojiScript

EmojiScript is a language made for the course ATP at the HU University of Applied Sciences Utrecht.  
The language consists entirely out of emoji and the lexer, parser and interpreter are mostly written with functional programming in mind.


## Example
```
🔢🅽⬅️1️⃣0️⃣0️⃣🔚
🔢🅰️⬅️0️⃣🔚
🔢🅱️⬅️1️⃣🔚
🔁👉🅽🔎0️⃣👈
⤵️
	🔢🅲⬅️🅰️➕🅱️🔚
	🅰️⬅️🅱️🔚
	🅱️⬅️🅲🔚
	🅽📉🔚
	🖨️🅰️🔚
⤴️
```
The above code will calculate and print the first 100 fibonacci numbers.


## How to use
EmojiScript can be used in 2 different ways.  
The first is via a shell, similarly to that of Python.  
It can be started simply by `python emojiscript.py`

The other way is to run an EmojiScript file. An EmojiScript file can either end in `.es` or `.📃`  
The file can then be run by `python emojiscript.py <filename>`


## Syntax
The items that are strikedthrough are planned and not yet implementted.
### Types:
* 🔢 -> Int
* 🎈 -> Float
* 🔠 -> String
* ✌️ -> Boolean
* 🌌 -> Void

### Literals:
* 👍 -> True
* 👎 -> False

### Binary operators:
* ➕ -> Plus
* ➖ -> Minus
* ✖️ -> Multiply
* ➗ -> Divide
* ⚡ -> Power
* ⬅️ -> Assign right to left
* ~~➡️ -> Assign left to right~~
* 🔀 -> Swap
* 🔬 -> Equals
* 🙅 -> Not equals
* 🔎 -> Greater than
* 🔍 -> Less than
* 🔎🔬 -> Greater or equal than
* 🔍🔬 -> Less or equal than
* 🤝 -> And
* 🤷 -> Or

### Unary operators:
* 📈 -> ++
* 📉 -> --

### Logic:
* 🤔 -> If statement
* 🔁 -> While loop
* 🥪 -> Sub(routine)
* 👉 -> Left parentheses
* 👈 -> Right parentheses
* ⤵️ -> Left brace
* ⤴️ -> Right brace

### Other:
* 🔚 -> End of statement(;)
* ~~↩️ -> Return~~
* 🖨️ -> Print
* ⌨️ -> Input
* ~~🛏 -> Sleep~~
* ~~😳 -> Error~~
* 📜 -> Comment
* ~~📖 -> Start multiline comment~~
* ~~📕 -> End multiline comment~~
