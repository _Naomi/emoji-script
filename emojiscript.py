﻿import re
import sys
from typing import List, Tuple, Callable, Union
from enum import Enum
import copy

sys.setrecursionlimit(10000)

#############################
#     LOGGING/DECORATOR     #
#############################
ENABLE_LOGGING = False # Toggle this if you like your commandline to be filled up with spam ;)

def LoggerDecorator(func):
    """Logger decorator which could be used while debugging."""
    if not ENABLE_LOGGING:
        return func

    def wrapper(*args, **kwargs):
        print("==============================")
        print(f"Func: {func.__name__}, Args: {args}")
        value = func(*args, **kwargs)
        return value

    return wrapper


##################
#     ERRORS     #
##################

class ErrorCode(Enum):
    NO_REGEX_IDENTIFIERS = "At least 1 RegEx needs to be defined"
    UNEXPECTED_CHARACTER = "Unexpected character"
    UNEXPECTED_TOKEN = "Unexpected token"
    UNEXPECTED_EOF = "Unexpected end-of-file"
    REF_BEFORE_ASSIGN = "Variable referenced before assignment"
    INCOMPATABLE_VALUE = "Incompatable value found"

class Error(Exception):
    def __init__(self, error_code):
        self.error_code = error_code

class LexerError(Error):
    def __init__(self, error_code):
        super().__init__(error_code)

class ParserError(Error):
    def __init__(self, error_code, token=None):
        super().__init__(error_code)
        self.token = token

class InterpreterError(Error):
    def __init__(self, error_code, message=None):
        super().__init__(error_code)
        self.message = message


###################
#     HELPERS     #
###################

number_dict = {'0️⃣':0,'1️⃣':1,'2️⃣':2,'3️⃣':3,'4️⃣':4,
               '5️⃣':5,'6️⃣':6,'7️⃣':7,'8️⃣':8,'9️⃣':9}

# emoji_to_int :: Str -> Int
def emoji_to_int(digits : str) -> int:
    """Convert string of number emojis to integer.

    A keycap number emoji consists of 3 seperate characters:
    A number, a Variation Selector-16 and a Combining Enclosing Keycap emoji.
    This function checks for every 3 characters whether they are a keycap number emoji
    and concats that with the result.
    It breaks if given a string without only keycap number emojis. (For now)
    """
    result = ''
    for i in range(int(len(digits)/3)):
        result += str(number_dict[digits[i*3:i*3+3]])
    return int(result)

# emoji_to_float :: Str -> Float
def emoji_to_float(float_digits : str) -> float:
    """Convert string of number emojis to float.

    This function will seperate the number into the interger part and the fractional part,
    then convert them to numbers, and then stitch them back together.
    """
    parts = float_digits.split('🎈')
    return float(emoji_to_int(parts[0])+emoji_to_int(parts[1])/10**(len(parts[1])/3))

# This variable holds all the variables created by EmojiScript.
variables = dict()


##################
#     TOKENS     #
##################

class Token(object):
    def __init__(self, type, value, line = None, column = None):
        self.type = type
        self.value = value
        self.line = line
        self.column = column

    def __repr__(self):
        return f'Token<{self.type}>({self.value}, pos={self.line}:{self.column})'


#################
#     LEXER     #
#################

@LoggerDecorator
# create_regex :: [Tuple[str, str]] -> Either Regex Error
def create_regex(regexes : List[Tuple[str, str]]) -> Union["Regex", "Error"]:
    """Create a master regex by combining each individual regex."""
    if (len(regexes) == 0):
        return LexerError(ErrorCode.NO_REGEX_IDENTIFIERS)

    return re.compile('|'.join(list(map(lambda reg: f"(?P<{reg[1]}>{reg[0]})", regexes))))

@LoggerDecorator
# next_token :: Str -> Regex -> Int -> Either (Token, Int) Error
def next_token(string : str, regex : "Regex", pos : int) -> Union[Tuple["Token", int], "Error"]:
    """Give token based on position in string.

    If no match for a token was found, only return next position.
    If a tokens was found, return that token and the next position at the end of the token.
    """
    match = regex.match(string, pos)
    if (match == None):
        if (string[pos] in ('\n', '\t', '\ufeff')):
            return (None, pos+1)
        return LexerError(ErrorCode.UNEXPECTED_CHARACTER)
    return (Token(match.lastgroup, match.group(match.lastgroup), line=string[:match.pos].count('\n')+1, column=match.pos-string[:match.pos].rfind('\n')), match.end())
    #TODO: Column tracking isn't totally accurate.

@LoggerDecorator
# tokenization :: Str -> Regex -> Int -> Either (Token, Int) Error
def tokenization(string : str, regex : "Regex", pos : int) -> Union[List["Token"], "Error"]:
    """Return a list of tokens from a given string.

    Given a string, a regex with matches and a starting position,
    this function with find all possible tokens in the string.
    """
    result = next_token(string, regex, pos)
    if (isinstance(result, Error)): # Error check
        return result
    token, new_pos = result
    if (new_pos >= len(string) and token == None):
        return []
    elif (new_pos >= len(string)):
        return [token]
    elif (token == None):
        return tokenization(string, regex, new_pos)
    else:
        result = tokenization(string, regex, new_pos)
        if (isinstance(result, Error)): # Error check
            return result
        return [token] + result


#################
#     NODES     #
#################

class Node(object):
    pass

class TypeNode(Node):
    def __init__(self, value):
        self.value = value

class IntNode(TypeNode):
    def __repr__(self):
        return f"INT:{self.value}"

class FloatNode(TypeNode):
    def __repr__(self):
        return f"FLOAT:{self.value}"

class StringNode(TypeNode):
    def __repr__(self):
        return f"STRING:{self.value}"

class BooleanNode(TypeNode):
    def __repr__(self):
        return f"BOOL:{self.value}"

class VarNode(Node):
    def __init__(self, token):
        self.token = token

    def __repr__(self):
        return f"{self.token.type}:{self.token.value}"

class BinOpNode(Node):
    def __init__(self, left, operator, right):
        self.left = left
        self.operator = operator
        self.right = right

    def __repr__(self):
        return f"({self.left}, {self.operator}, {self.right})"

class UnOpNode(Node):
    def __init__(self, token, expression):
        self.token = token
        self.expression = expression

    def __repr__(self):
        return f"({self.token.value}, {self.expression})"

class IncrementNode(Node):
    def __init__(self, id):
        self.id = id

    def __repr__(self):
        return f"({self.id}++)"

class DecrementNode(Node):
    def __init__(self, id):
        self.id = id

    def __repr__(self):
        return f"({self.id}--)"

class AssignNode(Node):
    def __init__(self, id, type_token, expression):
        self.id = id
        self.type_token = type_token
        self.expression = expression

    def __repr__(self):
        return f"({self.id}:{self.type_token.type if self.type_token else None} = {self.expression})"

class IfNode(Node):
    def __init__(self, expression, statements):
        self.expression = expression
        self.statements = statements

    def __repr__(self):
        return f"(IF:{self.expression}, {self.statements})"

class WhileNode(Node):
    def __init__(self, expression, statements):
        self.expression = expression
        self.statements = statements

    def __repr__(self):
        return f"(WHILE:{self.expression}, {self.statements})"

class FunctionNode(Node):
    def __init__(self, type, id, statements):
        self.type = type
        self.id = id
        self.statements = statements

    def __repr__(self):
        return f"(FUNC<{self.id}>:{self.statements})"

class FunctionCallNode(Node):
    def __init__(self, id):
        self.id = id

    def __repr__(self):
        return f"(CALL:{self.id})"

##################
#     PARSER     #
##################

@LoggerDecorator
# factor :: [Token] -> Int -> Either (Node, Int) Error
def factor(tokens : List["Token"], pos : int) -> Union[Tuple["Node", int], "Error"]:
    """Return a factor node.

    A factor node can be either a variable, an integer or an expression enclosed by parentheses.
    If none of these are found, return an error.
    """
    if (tokens[pos].type == "INTEGER"):
        return (IntNode(emoji_to_int(tokens[pos].value)), pos+1)
    elif(tokens[pos].type == "FLOAT"):
        return (FloatNode(emoji_to_float(tokens[pos].value)), pos+1)
    elif (tokens[pos].type == "STRING"):
        return (StringNode(tokens[pos].value), pos+1)
    elif (tokens[pos].type == "TRUE"):
        return (BooleanNode(True), pos+1)
    elif (tokens[pos].type == "FALSE"):
        return (BooleanNode(False), pos+1)
    elif (tokens[pos].type == "LPAREN"):
        result = expression(tokens, pos+1)

        if (isinstance(result, Error)): # Error check
            return result
        node, new_pos = result

        if (new_pos >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        if (tokens[new_pos].type != "RPAREN"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])

        return (node, new_pos+1)
    elif (tokens[pos].type == "IDENTIFIER"):
        return (VarNode(tokens[pos]), pos+1)
    elif (tokens[pos].type == "INPUT"):
        return (UnOpNode(tokens[pos], None), pos+1)

    else:
        return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[pos])

@LoggerDecorator
# find_operators :: [Token] -> Int ([Token] -> Int -> Either (Node, Int) Error) -> Set -> Maybe Node -> Either (Node, Int) Error
def find_operators(tokens : List["Token"], pos : int, func : Callable[[List["Token"], int], Union[Tuple["Node", int], "Error"]], set : set, left_node : "Node") -> Union[Tuple["Node", int], "Error"]:
    """Keep looking for operators.

    This function will keep looking for operators till it finds one that isn't in the set.
    Valid examples:
    (func)
    (func)(operator)(func)
    (func)(operator)(func)(operator)(func)
    etc.
    """
    if (left_node == None):
        result = func(tokens, pos)

        if (isinstance(result, Error)): # Error check
            return result
        left_node, new_pos = result
    else:
        new_pos = pos

    if (new_pos >= len(tokens)): # Range check
        return ParserError(ErrorCode.UNEXPECTED_EOF)
    
    if (tokens[new_pos].type in set):
        token = tokens[new_pos]

        result = func(tokens, new_pos+1)

        if (isinstance(result, Error)): # Error check
            return result
        right_node, new_pos = result

        return find_operators(tokens, new_pos, func, set, BinOpNode(left_node, token.type, right_node))

    return (left_node, new_pos)

@LoggerDecorator
# power :: [Token] -> Int -> Either (Node, Int) Error
def power(tokens : List["Token"], pos : int) -> Union[Tuple["Node", int], "Error"]:
    """Keep looking for power operators."""
    return find_operators(tokens, pos, factor, ("POWER"), None)

@LoggerDecorator
# term :: [Token] -> Int -> Either (Node, Int) Error
def term(tokens : List["Token"], pos : int) -> Union[Tuple["Node", int], "Error"]:
    """Keep looking for multiply and divide operators."""
    return find_operators(tokens, pos, power, ("MULTIPLY", "DIVIDE"), None)

@LoggerDecorator
# expression :: [Token] -> Int -> Either (Node, Int) Error
def expression(tokens : List["Token"], pos : int) -> Union[Tuple["Node", int], "Error"]:
    """Keep looking for plus, minus and logic operators."""
    return find_operators(tokens, pos, term, ("PLUS", "MINUS", "EQUALS", "NOT_EQUALS", "GREATER", "LESS", "GREATER_EQUALS", "LESS_EQUALS", "AND", "OR"), None)

@LoggerDecorator
# statement :: [Token] -> Int -> Either (Node, Int) Error
def statement(tokens : List["Token"], pos : int) -> Union[Tuple["Node", int], "Error"]:
    """Look whether a valid statement is defined at pos."""
    if (tokens[pos].type[:4] == "TYPE" and tokens[pos+1].type == "IDENTIFIER"):
        type_token = tokens[pos]

        if (pos+1 >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)

        id = tokens[pos+1].value

        if (pos+2 >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)

        if (tokens[pos+2].type == "LASSIGN"):
            result = expression(tokens, pos+3)

            if (isinstance(result, Error)): # Error check
                return result
            expr, new_pos = result

            if (new_pos >= len(tokens)): # Range check
                return ParserError(ErrorCode.UNEXPECTED_EOF)
            if (tokens[new_pos].type != "STAT_TERM"): # Correct token check
                return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])

            return (AssignNode(id, type_token, expr), new_pos+1)

        elif (tokens[pos+2].type == "STAT_TERM"): # Correct token check
            return (AssignNode(id, type_token, None), pos+3)

        return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])

    elif (tokens[pos].type == "IDENTIFIER" and pos+1 < len(tokens) and tokens[pos+1].type in ("INCREMENT","DECREMENT","LASSIGN","LPAREN","SWAP")):
        id = tokens[pos].value
        if (pos+1 >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        if (tokens[pos+1].type == "LASSIGN"):
            result = expression(tokens, pos+2)

            if (isinstance(result, Error)): # Error check
                return result
            expr, new_pos = result

            if (new_pos >= len(tokens)): # Range check
                return ParserError(ErrorCode.UNEXPECTED_EOF)
            if (tokens[new_pos].type != "STAT_TERM"): # Correct token check
                return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])

            return (AssignNode(id, None, expr), new_pos+1)

        if (tokens[pos+1].type == "LPAREN"):
            #TODO: Implement function arguments.

            if (tokens[pos+2].type != "RPAREN"): # Correct token check
                return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[pos+2])
            if (tokens[pos+3].type != "STAT_TERM"): # Correct token check
                return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[pos+3])

            return (FunctionCallNode(id), pos+4)

        if (tokens[pos+1].type == "SWAP"):
            if (pos+3 >= len(tokens)): # Range check
                return ParserError(ErrorCode.UNEXPECTED_EOF)
            if (tokens[pos+2].type != "IDENTIFIER"): # Correct token check
                return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[pos+3])
            if (tokens[pos+3].type != "STAT_TERM"): # Correct token check
                return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[pos+3])

            return (BinOpNode(tokens[pos], "SWAP", tokens[pos+2]), pos+4)

        if (pos+2 >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        if (tokens[pos+2].type != "STAT_TERM"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[pos+2])

        if (tokens[pos+1].type == "INCREMENT"):
            return (IncrementNode(id), pos+3)
        if (tokens[pos+1].type == "DECREMENT"):
            return (DecrementNode(id), pos+3)

    elif (tokens[pos].type == "PRINT"):
        token = tokens[pos]
        result = expression(tokens, pos+1)

        if (isinstance(result, Error)): # Error check
            return result
        expr, new_pos = result

        if (new_pos >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        if (tokens[new_pos].type != "STAT_TERM"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])

        return (UnOpNode(token, expr), new_pos+1)

    elif (tokens[pos].type == "IF_STAT"):
        if (pos+1 >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        if (tokens[pos+1].type != "LPAREN"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[pos+1])

        result = expression(tokens, pos+2)

        if (isinstance(result, Error)): # Error check
            return result
        expr, new_pos = result

        if (new_pos+3 >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        if (tokens[new_pos].type != "RPAREN"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])
        if (tokens[new_pos+1].type != "LBRACE"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos+1])

        result = get_statements(tokens, new_pos+2)

        if (isinstance(result, Error)): # Error check
            return result
        stats, new_pos = result

        if (new_pos >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        if (tokens[new_pos].type != "RBRACE"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])

        return (IfNode(expr, stats), new_pos+1)

    elif (tokens[pos].type == "WHILE_LOOP"):
        if (pos+1 >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        if (tokens[pos+1].type != "LPAREN"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])

        result = expression(tokens, pos+2)

        if (isinstance(result, Error)): # Error check
            return result
        expr, new_pos = result

        if (new_pos+3 >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        if (tokens[new_pos].type != "RPAREN"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])
        if (tokens[new_pos+1].type != "LBRACE"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos+1])

        result = get_statements(tokens, new_pos+2)

        if (isinstance(result, Error)): # Error check
            return result
        stats, new_pos = result

        if (new_pos >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        if (tokens[new_pos].type != "RBRACE"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])

        return (WhileNode(expr, stats), new_pos+1)

    elif (tokens[pos].type == "FUNCTION"):
        if (pos+3 >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        type = tokens[pos+1]
        id = tokens[pos+2]
        if (tokens[pos+3].type != "LPAREN"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[pos+3])

        #TODO: Implement function arguments

        if (tokens[pos+4].type != "RPAREN"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[pos+4])
        if (tokens[pos+5].type != "LBRACE"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[pos+5])
        
        result = get_statements(tokens, pos+6)

        if (isinstance(result, Error)): # Error check
            return result
        stats, new_pos = result

        if (tokens[new_pos].type != "RBRACE"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])
        
        return (FunctionNode(type.type, id.value, stats), new_pos+1)

    else:
        result = expression(tokens, pos)

        if (isinstance(result, Error)): # Error check
            return result
        expr, new_pos = result

        if (new_pos >= len(tokens)): # Range check
            return ParserError(ErrorCode.UNEXPECTED_EOF)
        if (tokens[new_pos].type != "STAT_TERM"): # Correct token check
            return ParserError(ErrorCode.UNEXPECTED_TOKEN, tokens[new_pos])

        return (expr, new_pos+1)


@LoggerDecorator
# get_statements :: [Token] -> Int -> Either (Node, Int) Error
def get_statements(tokens : List["Token"], pos : int) -> Union[Tuple[List["Node"], int], "Error"]:
    """Make a list of AST's.

    This function will return a list of Abstract syntax trees.
    1 AST for each statement.
    It will keep going till the EOF or a right brace.
    """
    if (pos+1 >=  len(tokens) or tokens[pos].type == "RBRACE"):
        return ([], pos)
    if (tokens[pos].type == "COMMENT"):
        return get_statements(tokens, pos+1)
    result = statement(tokens, pos)

    if (isinstance(result, Error)): # Error check
        return result
    stat, new_pos = result

    result = get_statements(tokens, new_pos)

    if (isinstance(result, Error)): # Error check
        return result
    stats, new_pos = result

    return ([stat] + stats, new_pos)


#######################
#     INTERPRETER     #
#######################

@LoggerDecorator
# interpret :: Either Node [Node] -> Either Node Error
def interpret(node : Union["Node", List["Node"]]) -> Union["Node", "Error"]:
    """Interpret a given node."""
    if (isinstance(node, list) and len(node) > 0):
        list(map(interpret, node))
        #TODO: Do something with this output?
    elif (isinstance(node, BinOpNode)):
        if (node.operator == "PLUS"):
            left, right = interpret(node.left), interpret(node.right)
            if (isinstance(left, IntNode) and isinstance(right, IntNode)):
                return IntNode(left.value + right.value)
            elif ((isinstance(left, IntNode) or isinstance(left, FloatNode)) and (isinstance(right, IntNode) or isinstance(right, FloatNode))):
                return FloatNode(left.value + right.value)
            elif (isinstance(left, StringNode) and isinstance(right, StringNode)):
                return StringNode(left.value[:-1]+right.value[1:])
            return InterpreterError(ErrorCode.INCOMPATABLE_VALUE, f"Can't add {type(left)} and {type(right)}")
        elif (node.operator == "MINUS"):
            left, right = interpret(node.left), interpret(node.right)
            if (isinstance(left, IntNode) and isinstance(right, IntNode)):
                return IntNode(left.value - right.value)
            elif ((isinstance(left, IntNode) or isinstance(left, FloatNode)) and (isinstance(right, IntNode) or isinstance(right, FloatNode))):
                return FloatNode(left.value - right.value)
            return InterpreterError(ErrorCode.INCOMPATABLE_VALUE, f"Can't subtract {type(left)} and {type(right)}")
        elif (node.operator == "MULTIPLY"):
            left, right = interpret(node.left), interpret(node.right)
            if (isinstance(left, IntNode) and isinstance(right, IntNode)):
                return IntNode(left.value * right.value)
            elif ((isinstance(left, IntNode) or isinstance(left, FloatNode)) and (isinstance(right, IntNode) or isinstance(right, FloatNode))):
                return FloatNode(left.value * right.value)
            return InterpreterError(ErrorCode.INCOMPATABLE_VALUE, f"Can't multiply {type(left)} and {type(right)}")
        elif (node.operator == "DIVIDE"):
            left, right = interpret(node.left), interpret(node.right)
            if ((isinstance(left, IntNode) or isinstance(left, FloatNode)) and (isinstance(right, IntNode) or isinstance(right, FloatNode))):
                return FloatNode(left.value / right.value)
            return InterpreterError(ErrorCode.INCOMPATABLE_VALUE, f"Can't divide {type(left)} and {type(right)}")
        elif (node.operator == "POWER"):
            left, right = interpret(node.left), interpret(node.right)
            if (isinstance(left, IntNode) and isinstance(right, IntNode)):
                return IntNode(left.value ** right.value)
            elif ((isinstance(left, IntNode) or isinstance(left, FloatNode)) and (isinstance(right, IntNode) or isinstance(right, FloatNode))):
                return FloatNode(left.value ** right.value)
            return InterpreterError(ErrorCode.INCOMPATABLE_VALUE, f"Can't calculate power of {type(left)} and {type(right)}")
        elif (node.operator == "EQUALS"):
            return BooleanNode(interpret(node.left).value == interpret(node.right).value)
        elif (node.operator == "NOT_EQUALS"):
            return BooleanNode(interpret(node.left).value != interpret(node.right).value)
        elif (node.operator == "GREATER"):
            return BooleanNode(interpret(node.left).value > interpret(node.right).value)
        elif (node.operator == "LESS"):
            return BooleanNode(interpret(node.left).value < interpret(node.right).value)
        elif (node.operator == "GREATER_EQUALS"):
            return BooleanNode(interpret(node.left).value >= interpret(node.right).value)
        elif (node.operator == "LESS_EQUALS"):
            return BooleanNode(interpret(node.left).value <= interpret(node.right).value)
        elif(node.operator == "SWAP"):
            if (node.left.value not in variables or node.right.value not in variables):
                return InterpreterError(ErrorCode.REF_BEFORE_ASSIGN)
            if (type(variables[node.left.value]) != type(variables[node.right.value])):
                return InterpreterError(ErrorCode.INCOMPATABLE_VALUE, "Both variables need to be the same to be able to be swapped.")
            variables[node.left.value].value, variables[node.right.value].value = variables[node.right.value].value, variables[node.left.value].value
        elif(node.operator == "AND"):
            return BooleanNode(interpret(node.left).value and interpret(node.right).value)
        elif(node.operator == "OR"):
            return BooleanNode(interpret(node.left).value or interpret(node.right).value)

    elif (isinstance(node, TypeNode)):
        return node
    elif (isinstance(node, UnOpNode)):
        if (node.token.type == "PRINT"):
            result = interpret(node.expression)
            if (isinstance(result, Error)): # Error check
                return result
            print(result.value)
        elif (node.token.type == "INPUT"):
            return StringNode("🔠" + input() + "🔠")
    elif (isinstance(node, IncrementNode)):
        variables[node.id].value += 1
    elif (isinstance(node, DecrementNode)):
        variables[node.id].value -= 1
    elif (isinstance(node, AssignNode)):
        variables[node.id] = copy.copy(interpret(node.expression))
    elif (isinstance(node, VarNode)):
        if (node.token.value in variables):
            return variables[node.token.value]
        else:
            return InterpreterError(ErrorCode.REF_BEFORE_ASSIGN)
    elif (isinstance(node, IfNode)):
        condition = interpret(node.expression)
        if (condition.value):
            return interpret(node.statements)
    elif (isinstance(node, WhileNode)):
        condition = interpret(node.expression)
        if (condition.value):
            result = interpret(node.statements)
            interpret(node)
    elif (isinstance(node, FunctionNode)):
        variables[node.id] = node
    elif (isinstance(node, FunctionCallNode)):
        interpret(variables[node.id].statements)



regexes = [
    # Literals
    ('🔠[^🔠]*🔠',                   'STRING'),
    ('[0️⃣1️⃣2️⃣3️⃣4️⃣5️⃣6️⃣7️⃣8️⃣9️⃣]+🎈[0️⃣1️⃣2️⃣3️⃣4️⃣5️⃣6️⃣7️⃣8️⃣9️⃣]+', 'FLOAT'),
    ('[0️⃣1️⃣2️⃣3️⃣4️⃣5️⃣6️⃣7️⃣8️⃣9️⃣]+',                'INTEGER'),
    ('👍',                           'TRUE'),
    ('👎',                           'FALSE'),
    # Types
    ('🔢',                           'TYPE_INT'),
    ('🎈',                           'TYPE_FLOAT'),
    ('🔤',                           'TYPE_STRING'),
    ('✌️',                            'TYPE_BOOLEAN'),
    ('🌌',                           'TYPE_VOID'),
    # Operators
    ('➕',                           'PLUS'),
    ('➖',                           'MINUS'),
    ('✖️',                            'MULTIPLY'),
    ('➗',                           'DIVIDE'),
    ('⚡',                           'POWER'),
    ('📈',                           'INCREMENT'),
    ('📉',                           'DECREMENT'),
    ('🔬',                           'EQUALS'),
    ('🙅',                           'NOT_EQUALS'),
    ('🔎🔬',                         'GREATER_EQUALS'),
    ('🔍🔬',                         'LESS_EQUALS'),
    ('🔎',                           'GREATER'),
    ('🔍',                           'LESS'),
    ('⬅️',                            'LASSIGN'),
    ('➡️',                            'RASSIGN'),
    ('🔀',                           'SWAP'),
    ('🤝',                           'AND'),
    ('🤷',                           'OR'),
    # Logic
    ('🤔',                           'IF_STAT'),
    ('🔁',                           'WHILE_LOOP'),
    ('👉',                           'LPAREN'),
    ('👈',                           'RPAREN'),
    ('⤵️',                            'LBRACE'),
    ('⤴️',                            'RBRACE'),
    ('🥪',                           'FUNCTION'),
    # Other
    ('[\U0001F170-\U0001F189🅰️🅱️🅾️🅿️]+', 'IDENTIFIER'),
    ('🔚',                           'STAT_TERM'),
    ('📜[^\n]*',                     'COMMENT'),
    ('🖨️',                           'PRINT'),
    ('⌨️',                            'INPUT'),
]


def emoji_script(string, regex, debug=False):
    tokens = tokenization(string, regex, 0)
    if (isinstance(tokens, Error)):
        return tokens
    if (debug):
        print(f"Tokens: \n{tokens}")

    statements = get_statements(tokens, 0)
    if (isinstance(statements, Error)):
        return statements
    asts, _ = statements
    if (debug):
        print("AST's:")
        print(*asts, sep='\n')

    result = interpret(asts)
    if (isinstance(result, Error)):
        return result
    if (debug):
        print(f"Interpreter result: {result}")


if __name__ == "__main__":
    regex = create_regex(regexes)
    if (isinstance(regex, Error)):
        print("There needs to be at least 1 RegEx")
        quit()

    if (len(sys.argv) == 1):
        while True:
            emoji_script(input("ES > "), regex)
    else:
        if (sys.argv[1][-2:] != '.📃' and sys.argv[1][-3:] != '.es'):
            print("Error: EmojiScript files can only end with .📃 or .es")
            quit()
        try:
            f = open(sys.argv[1], 'r', encoding="utf8")
            string = f.read()[:-1]
            print(emoji_script(string, regex))
        except FileNotFoundError:
            print(f"Error: File '{sys.argv[1]}' doesn't exist")

